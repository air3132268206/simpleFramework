package org.simpleFramework.util;


import org.junit.jupiter.api.*;

import java.util.Set;

/**
 * @description:
 * @author: air
 * @create: 2020-03-16 10:12
 */

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ClassUtilTest {

    @DisplayName(value = "提取目标类方法")
    @Test
    public void extractPackageClass(){
        Set<Class<?>> classSet = ClassUtil.extractPackageClass("cn.airfei.controller");
        System.out.println(classSet);

        Assertions.assertEquals(4,classSet.size());

    }
}
