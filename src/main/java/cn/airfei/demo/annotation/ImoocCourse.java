package cn.airfei.demo.annotation;

/**
 * @description:
 * @author: air
 * @create: 2020-03-14 15:33
 */
@CourseInfoAnnotation(courseName = "haha",courseTag = "tag",courseIndex = 12)
public class ImoocCourse {

    @PersonInfoAnnotation(name = "air",gender = "nan",language = {"12"})
    private String author;

    @CourseInfoAnnotation(courseName = "mmmmmm",courseTag = "kkkk")
    public void getCourseInfo(){

    }

}
