package cn.airfei.demo.pattern;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @description:
 * @author: air
 * @create: 2020-03-16 10:42
 */
public class SingletonMain {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        System.out.println(EnumStarvingSingleton.getInstance());

        Class clazz = EnumStarvingSingleton.class;
        Constructor constructor=clazz.getDeclaredConstructor();
        constructor.setAccessible(true);
        EnumStarvingSingleton enumStarvingSingleton= (EnumStarvingSingleton) constructor.newInstance();
        System.out.println(enumStarvingSingleton.getInstance());
        System.out.println(((EnumStarvingSingleton) constructor.newInstance()).getInstance());








    }

}
