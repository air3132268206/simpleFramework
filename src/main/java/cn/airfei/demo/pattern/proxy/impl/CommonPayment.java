package cn.airfei.demo.pattern.proxy.impl;

/**
 * @description:
 * @author: air
 * @create: 2020-05-09 11:30
 */
public class CommonPayment {

    public void pay(){
        System.out.println("common pay");
    }

}
