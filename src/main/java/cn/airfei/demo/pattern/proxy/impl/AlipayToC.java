package cn.airfei.demo.pattern.proxy.impl;

import cn.airfei.demo.pattern.proxy.ToCPayment;

/**
 * @description:
 * @author: air
 * @create: 2020-05-09 10:38
 */
public class AlipayToC implements ToCPayment {
    ToCPayment toCPayment;

    public AlipayToC(ToCPayment toCPayment){
        this.toCPayment=toCPayment;
    }


    @Override
    public void pay() {
        beforePay();
        toCPayment.pay();
        afterPay();
    }

    private void afterPay() {
        System.out.println("支付给慕课网");
    }

    private void beforePay() {
        System.out.println("从招商银行取款");
    }
}
