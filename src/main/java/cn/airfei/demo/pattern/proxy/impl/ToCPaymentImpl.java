package cn.airfei.demo.pattern.proxy.impl;

import cn.airfei.demo.pattern.proxy.ToCPayment;

/**
 * @description:
 * @author: air
 * @create: 2020-05-09 10:37
 */
public class ToCPaymentImpl implements ToCPayment {

    @Override
    public void pay() {
        System.out.println("test toc payment");
    }
}
