package cn.airfei.demo.pattern.eventmode;

/**
 * @description:
 * @author: air
 * @create: 2020-05-06 10:42
 */
public class EventModeDemo {

    public static void main(String[] args) {
        EventSource eventSource=new EventSource();

        SingleClickEventListener singleClickEventListener=new SingleClickEventListener();
        DoubleClickEventListener doubleClickEventListener=new DoubleClickEventListener();

        Event event=new Event();

        event.setType("doubleclick");
        eventSource.register(singleClickEventListener);
        eventSource.register(doubleClickEventListener);

        eventSource.publishEvent(event);
    }

}
