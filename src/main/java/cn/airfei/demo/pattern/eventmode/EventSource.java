package cn.airfei.demo.pattern.eventmode;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: air
 * @create: 2020-05-06 10:39
 */
public class EventSource {

    private List<EventListener> listenerList=new ArrayList<>();

    public void register(EventListener eventListener){
        listenerList.add(eventListener);
    }

    /**
     * 发布时间
     * @param event
     */
    public void publishEvent(Event event){
        for (EventListener eventListener:listenerList){
            eventListener.processEvent(event);
        }
    }



}
