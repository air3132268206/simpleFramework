package cn.airfei.demo.pattern.eventmode;

/**
 * @description:
 * @author: air
 * @create: 2020-05-06 10:38
 */
public class DoubleClickEventListener implements EventListener{


    @Override
    public void processEvent(Event event) {
        if ("doubleclick".equals(event.getType())){
            System.out.println("双击事件被触发");
        }
    }
}
