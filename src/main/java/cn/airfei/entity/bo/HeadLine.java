package cn.airfei.entity.bo;

import lombok.Data;

import java.util.Date;

/**
 * @description:
 * @author: air
 * @create: 2020-03-13 17:01
 */
@Data
public class HeadLine {
    private Long lineId;
    private String lineName;
    private String lineLink;
    private String lineImg;
    // 优先级
    private Integer priority;
    private Integer enableStatus;
    private Date createTime;
    private Date lastEditTime;

}
