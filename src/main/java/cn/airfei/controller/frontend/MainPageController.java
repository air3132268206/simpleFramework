package cn.airfei.controller.frontend;

import cn.airfei.entity.dto.MainPageInfoDto;
import cn.airfei.entity.dto.Result;
import cn.airfei.service.combine.IHeadLineShopCategoryCombineService;
import lombok.Getter;
import org.simpleFramework.core.annotation.Controller;
import org.simpleFramework.inject.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @description:
 * @author: air
 * @create: 2020-03-14 11:22
 */
@Controller
@Getter
public class MainPageController {
    @Autowired
    private IHeadLineShopCategoryCombineService iHeadLineShopCategoryCombineService;

    public Result<MainPageInfoDto> getMainPageInfo(HttpServletRequest request, HttpServletResponse response){
        return iHeadLineShopCategoryCombineService.getMainPageInfo();
    }

}
