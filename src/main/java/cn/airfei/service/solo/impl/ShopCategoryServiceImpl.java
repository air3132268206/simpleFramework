package cn.airfei.service.solo.impl;

import cn.airfei.entity.bo.ShopCategory;
import cn.airfei.entity.dto.Result;
import cn.airfei.service.solo.IShopCategoryService;
import org.simpleFramework.core.annotation.Service;

import java.util.List;

/**
 * @description:
 * @author: air
 * @create: 2020-03-14 10:28
 */
@Service
public class ShopCategoryServiceImpl implements IShopCategoryService {

    public Result<Boolean> addShopCategory(ShopCategory shopCategory) {
        return null;
    }

    public Result<Boolean> removeShopCategory(ShopCategory shopCategory) {
        return null;
    }

    public Result<Boolean> modifyShopCategory(ShopCategory shopCategory) {
        return null;
    }

    public Result<ShopCategory> getShopCategoryById(int shopCategoryId) {
        return null;
    }

    public Result<List<ShopCategory>> getShopCategoryList(ShopCategory shopCategoryCondition, int pageIndex, int pageSize) {
        return null;
    }
}
