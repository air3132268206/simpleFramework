package cn.airfei.service.combine.impl;

import cn.airfei.entity.bo.HeadLine;
import cn.airfei.entity.bo.ShopCategory;
import cn.airfei.entity.dto.MainPageInfoDto;
import cn.airfei.entity.dto.Result;
import cn.airfei.service.combine.IHeadLineShopCategoryCombineService;
import cn.airfei.service.solo.IHeadLineService;
import cn.airfei.service.solo.IShopCategoryService;
import org.simpleFramework.core.annotation.Service;
import org.simpleFramework.inject.annotation.Autowired;

import java.util.List;

/**
 * @description:
 * @author: air
 * @create: 2020-03-14 10:31
 */
@Service
public class HeadLineShopCategoryCombineServiceImpl implements IHeadLineShopCategoryCombineService {
    @Autowired
    private IHeadLineService iHeadLineService;

    @Autowired
    private IShopCategoryService iShopCategoryService;

    public Result<MainPageInfoDto> getMainPageInfo() {
        HeadLine headLine = new HeadLine();
        headLine.setEnableStatus(1);
        Result<List<HeadLine>> headLineRes = iHeadLineService.getHeadLineList(headLine, 1, 4);

        ShopCategory shopCategory = new ShopCategory();
        Result<List<ShopCategory>> shopCategoryRes = iShopCategoryService.getShopCategoryList(shopCategory, 1, 100);

        MainPageInfoDto mainPageInfoDto=new MainPageInfoDto();
        mainPageInfoDto.setHeadLineList(headLineRes.getData());
        mainPageInfoDto.setShopCategoryList(shopCategoryRes.getData());

        Result<MainPageInfoDto> result=new Result();
        result.setData(mainPageInfoDto);
        return result;
    }
}
