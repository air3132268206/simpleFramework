package org.simpleFramework.aop;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.simpleFramework.aop.aspect.AspectInfo;
import org.simpleFramework.util.ValidationUtil;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * @description:
 * @author: air
 * @create: 2020-05-09 15:39
 */
public class AspectListExecutor implements MethodInterceptor {
    private Class<?> targetClass;

    private List<AspectInfo> aspectInfoList;

    public AspectListExecutor(Class<?> targetClass, List<AspectInfo> aspectInfoList) {
        this.targetClass = targetClass;
        this.aspectInfoList = sortAspectInfoList(aspectInfoList);
    }

    /**
     * 升序排序
     * @param aspectInfoList
     * @return
     */
    private List<AspectInfo> sortAspectInfoList(List<AspectInfo> aspectInfoList) {
        Collections.sort(aspectInfoList, new Comparator<AspectInfo>() {
            @Override
            public int compare(AspectInfo o1, AspectInfo o2) {
                return o1.getOrderIndex()-o2.getOrderIndex();
            }
        });
        return aspectInfoList;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        Object returnValue=null;
        collectAccurateMatchedAspectList(method);
        if (ValidationUtil.isEmpty(aspectInfoList)){
            // 2.执行被代理类的方法
            returnValue=methodProxy.invokeSuper(methodProxy,args);
            return returnValue;
        }
        // 1.按照order的顺序升序执行完所有aspect的before方法
        invokeBeforeAdvices(method,args);
        try {
            // 2.执行被代理类的方法
            returnValue=methodProxy.invokeSuper(methodProxy,args);
            // 3.如果被代理类方法正常返回，这按照order的顺序降序执行完所有Aspect的afterReturning方法
            returnValue=invokeAfterReturningAdvices(method,args,returnValue);
        }catch (Exception e){
            // 4.如果被代理类方法抛出异常，则按照order的顺序降序执行完所有Aspect的afterThrowing方法
            invokeAfterThrowingAdvices(method,args,e);
        }

        return returnValue;
    }

    private void collectAccurateMatchedAspectList(Method method) {
        if (ValidationUtil.isEmpty(aspectInfoList)){
            return;
        }
        Iterator<AspectInfo> it=aspectInfoList.iterator();
        while (it.hasNext()){
            AspectInfo aspectInfo=it.next();
            if(!aspectInfo.getPointcutLocator().accurateMatches(method)){
                it.remove();
            }
        }
    }

    private void invokeAfterThrowingAdvices(Method method, Object[] args, Exception e) throws Throwable {
        for (int i=aspectInfoList.size()-1;i>=0;i--){
            aspectInfoList.get(i).getDefaultAspect().afterReturning(targetClass,method,args,e);
        }
    }

    private Object invokeAfterReturningAdvices(Method method, Object[] args, Object returnValue) throws Throwable {
        Object result=null;

        for (int i=aspectInfoList.size()-1;i>=0;i--){
            result=aspectInfoList.get(i).getDefaultAspect().afterReturning(targetClass,method,args,returnValue);
        }
        return result;
    }

    private void invokeBeforeAdvices(Method method, Object[] args) throws Throwable {
        for (AspectInfo aspectInfo:aspectInfoList){
            aspectInfo.getDefaultAspect().before(targetClass,method,args);
        }
    }
}
