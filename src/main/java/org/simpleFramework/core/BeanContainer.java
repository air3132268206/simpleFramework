package org.simpleFramework.core;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.simpleFramework.aop.annotation.Aspect;
import org.simpleFramework.core.annotation.Component;
import org.simpleFramework.core.annotation.Controller;
import org.simpleFramework.core.annotation.Repository;
import org.simpleFramework.core.annotation.Service;
import org.simpleFramework.util.ClassUtil;
import org.simpleFramework.util.ValidationUtil;

import java.lang.annotation.Annotation;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description: 获取bean容器实例
 * @author: air
 * @create: 2020-03-16 11:20
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BeanContainer {
    // 容器是否被加载过
    private boolean loading = false;

    // 存放所有被配置标记的目标对象的map
    private final Map<Class<?>, Object> beanMap = new ConcurrentHashMap();

    // 指定获取class的范围
    private static final List<Class<? extends Annotation>> BEAN_ANNOTATION = Arrays.asList(Component.class, Controller.class, Service.class, Repository.class, Aspect.class);


    public static BeanContainer getInstance() {
        return ContainerHolder.HOLDER.instance;
    }

    public boolean isLoading() {
        return loading;
    }

    /**
     * 加载实例
     *
     * @param packageName
     */
    public synchronized void loadBeans(String packageName) {
        if (loading) {
            log.warn("容器已经被加载过");
            return;
        }
        Set<Class<?>> classSet = ClassUtil.extractPackageClass(packageName);
        if (ValidationUtil.isEmpty(classSet)) {
            log.warn("没有找到相应的包:{}", packageName);
            return;
        }
        for (Class c : classSet) {
            for (Class<? extends Annotation> annotation : BEAN_ANNOTATION) {
                if (c.isAnnotationPresent(annotation)) {
                    beanMap.put(c, ClassUtil.newInstance(c, true));
                }
            }
        }
        loading = true;
    }

    /**
     * beanMap
     *
     * @return
     */
    public int size() {
        return beanMap.size();
    }

    /**
     * 添加一个class对象及其bean的实例
     *
     * @param clazz
     * @param bean
     * @return
     */
    public Object addBean(Class<?> clazz, Object bean) {
        return beanMap.put(clazz, bean);
    }

    /**
     * 移除一个ioc容器管理的对象
     *
     * @param clazz
     * @return
     */
    public Object removeBean(Class<?> clazz) {
        return beanMap.remove(clazz);
    }

    /**
     * 获取一个bean实例
     *
     * @param clazz
     * @return
     */
    public Object getBean(Class<?> clazz) {
        return beanMap.get(clazz);
    }

    /**
     * 获取容器管理的所有class对象集合
     *
     * @return
     */
    public Set<Class<?>> getClasses() {
        return beanMap.keySet();
    }

    /**
     * 获取所有bean的集合
     *
     * @return
     */
    public Set<Object> getBeans() {
        return new HashSet<Object>(beanMap.values());
    }

    /**
     * 根据注释筛选bean的class集合
     *
     * @param annotation
     * @return
     */
    public Set<Class<?>> getClassesByAnnotation(Class<? extends Annotation> annotation) {
        Set<Class<?>> keySet = getClasses();
        if (ValidationUtil.isEmpty(keySet)) {
            log.warn("没有任何class对象");
            return null;
        }
        Set<Class<?>> classSet = new HashSet<>();
        for (Class<?> c : keySet) {
            if (c.isAnnotationPresent(annotation)) {
                classSet.add(c);
            }
        }
        return classSet.size() > 0 ? classSet : null;
    }


    /**
     * 通过接口或父类或获取实现类或子类的集合，不包括其本身
     *
     * @param interfaceOrClass
     * @return
     */
    public Set<Class<?>> getClassesBySuper(Class<?> interfaceOrClass) {
        Set<Class<?>> keySet = getClasses();
        if (ValidationUtil.isEmpty(keySet)) {
            log.warn("没有任何class对象");
            return null;
        }

        Set<Class<?>> classSet = new HashSet<>();
        for (Class<?> c : keySet) {
            // 判断keySet里的元素是否是传入的接口或类的子类
            if (interfaceOrClass.isAssignableFrom(c) && !c.equals(interfaceOrClass)) {
                classSet.add(c);
            }
        }
        return classSet.size() > 0 ? classSet : null;
    }


    private enum ContainerHolder {
        HOLDER;
        private BeanContainer instance;

        ContainerHolder() {
            instance = new BeanContainer();
        }
    }


}
