package org.simpleFramework.inject;

import lombok.extern.slf4j.Slf4j;
import org.simpleFramework.core.BeanContainer;
import org.simpleFramework.inject.annotation.Autowired;
import org.simpleFramework.util.ClassUtil;
import org.simpleFramework.util.ValidationUtil;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Set;

/**
 * @description:
 * @author: air
 * @create: 2020-03-16 15:10
 */
@Slf4j
public class DependencyInject {
    // bean容器
    private BeanContainer beanContainer;

    public DependencyInject() {
        beanContainer = BeanContainer.getInstance();
    }

    /**
     * 执行ioc
     */
    public void doIoc(){
        if (ValidationUtil.isEmpty(beanContainer.getClasses())){
            log.warn("容器为空...");
            return;
        }
        for (Class<?> clazz : beanContainer.getClasses()){
            Field[] files=clazz.getDeclaredFields();
            if (ValidationUtil.isEmpty(files)){
                continue;
            }
            for (Field field:files){
                if(field.isAnnotationPresent(Autowired.class)){
                    Autowired autowired=field.getAnnotation(Autowired.class);
                    String autowiredValue=autowired.value();

                    // 获取成员变量的类型
                    Class<?> fieldClass=field.getType();
                    // 获取成员变量的类型在容器对应的实例
                    Object fieldValue=getFieldInstance(fieldClass,autowiredValue);
                    if (fieldValue == null) {
                        throw new RuntimeException("没有找到对应实例："+fieldValue.toString());
                    }else {
                        Object targetBean=beanContainer.getBean(clazz);
                        ClassUtil.setField(field,targetBean,fieldValue,true);
                    }


                }
            }
        }
    }

    /**
     * 根据class在beanContainer里获取取其实例或实现类
     * @param fieldClass
     * @return
     */
    private Object getFieldInstance(Class<?> fieldClass,String autowiredValue){
        Object fieldValue=beanContainer.getBean(fieldClass);
        if (fieldValue!=null){
            return fieldValue;
        }else {
            Class<?> implementedClass=getImplementedClass(fieldClass,autowiredValue);
            if (implementedClass!=null){
                return beanContainer.getBean(implementedClass);
            }else {
                return null;
            }
        }
    }


    /**
     * 获取接口的实现类
     * @param fieldClass
     * @param autowiredValue
     * @return
     */
    private Class<?> getImplementedClass(Class<?> fieldClass,String autowiredValue){
        Set<Class<?>> classSet=beanContainer.getClassesBySuper(fieldClass);
        if (!ValidationUtil.isEmpty(classSet)){
            if (ValidationUtil.isEmpty(autowiredValue)){
                if (classSet.size()==1){
                    return classSet.iterator().next();
                }else {
                    throw new RuntimeException("接口存在多个实现类，您需要指定@Autowired(value) 具体类："+fieldClass.getName());
                }
            }else {
                for (Class<?> c:classSet){
                    if(autowiredValue.equals(c.getSimpleName())){
                        return c;
                    }
                }
            }
        }
        return null;
    }
}
