package org.simpleFramework.mvc.render.impl;

import com.alibaba.fastjson.JSON;
import org.simpleFramework.mvc.RequestProcessorChain;
import org.simpleFramework.mvc.render.ResultRender;

import java.io.PrintWriter;

/**
 * @description:
 * @author: air
 * @create: 2020-05-14 14:55
 */
public class JsonResultRender implements ResultRender {
    private  Object jsonData;

    public JsonResultRender(Object result) {
        this.jsonData=result;
    }

    @Override
    public void render(RequestProcessorChain requestProcessorChain) throws Exception {
        requestProcessorChain.getResponse().setContentType("application/json");
        requestProcessorChain.getResponse().setCharacterEncoding("UTF-8");

        PrintWriter printWriter=requestProcessorChain.getResponse().getWriter();
        printWriter.write(JSON.toJSONString(this.jsonData));
        printWriter.flush();

    }
}
