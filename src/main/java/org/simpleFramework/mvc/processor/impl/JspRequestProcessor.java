package org.simpleFramework.mvc.processor.impl;

import org.simpleFramework.mvc.RequestProcessorChain;
import org.simpleFramework.mvc.processor.RequestProcessor;

import javax.servlet.ServletContext;

/**
 * @description:
 * @author: air
 * @create: 2020-05-14 14:39
 */
public class JspRequestProcessor implements RequestProcessor {

    public JspRequestProcessor(ServletContext servletContext) {

    }

    @Override
    public boolean process(RequestProcessorChain requestProcessorChain) throws Exception {
        return false;
    }
}
