package org.simpleFramework.mvc.type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: air
 * @create: 2020-05-14 16:45
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestPathInfo {
    private String httpMethod;

    private String httpPath;

}
