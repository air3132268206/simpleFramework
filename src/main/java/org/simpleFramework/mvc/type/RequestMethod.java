package org.simpleFramework.mvc.type;

public enum RequestMethod {
    GET,
    POST
}
